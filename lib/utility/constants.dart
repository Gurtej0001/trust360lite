class Constants {
  static const String app_name = "Trust360lite";

  static const String development_app = "Development App";

  static const String username = "Username";
  static const String email_id = "Email Id";
  static const String sign_in = "Sign In";

  static const String sms_otp = "SMS OTP";
  static const String email_otp = "Email OTP";
  static const String resend_otp = "Resend OTP";
  static const String verify_otp = "Verify OTP";

  static const String submit = "Submit";

  static const String my_favorites = "My Favorites";

  static const String subject = "Subject";
  static const String description = "Description";

  static const String current_buzz = "Current Buzz";
  static const String discussion = "Discussion";
  static const String knowledge_base = "Knowledge Base";

}
