import 'package:flutter/material.dart';
import 'package:trust360lite/view/current_buzz/capture_from_url.dart';
import 'package:trust360lite/view/current_buzz/enter_buzz.dart';
import 'package:trust360lite/view/cyber_news.dart';
import 'package:trust360lite/view/dashboard.dart';
import 'package:trust360lite/view/discussion/add_participants.dart';
import 'package:trust360lite/view/discussion/new_discussion.dart';
import 'package:trust360lite/view/help_and_feedback.dart';
import 'package:trust360lite/view/knowledge_base/add_knowledge_base.dart';
import 'package:trust360lite/view/knowledge_base/cyber_advisories.dart';
import 'package:trust360lite/view/my_activity/my_activity.dart';
import 'package:trust360lite/view/my_favorite/my_favorite_discussion_chat.dart';
import 'package:trust360lite/view/my_favorite/my_favorite_discussion_detail.dart';
import 'package:trust360lite/view/my_favorite/my_favorites.dart';
import 'package:trust360lite/view/notification/notification.dart' as notification;
import 'package:trust360lite/view/notification/notification_discussion.dart';
import 'package:trust360lite/view/notification/notification_discussion_chat.dart';
import 'package:trust360lite/view/notification/notification_discussion_detail.dart';
import 'package:trust360lite/view/notification/notification_discussion_update.dart';
import 'package:trust360lite/view/otp.dart';
import 'package:trust360lite/view/profile/edit_profile.dart';
import 'package:trust360lite/view/profile/profile.dart';
import 'package:trust360lite/view/search.dart';
import 'package:trust360lite/view/sign_in.dart';
import 'package:trust360lite/view/knowledge_base/view_knowledge_base.dart';

class NavigationController {
  static String signIn = "/signIn";
  static String otp = "/otp";
  static String root = "/";

  static Map<String, WidgetBuilder> configureRoutes() {
    var routes = <String, WidgetBuilder>{signIn: (BuildContext context) => SignIn()};
    return routes;
  }

  static void navigateToSignIn(BuildContext context) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignIn()));
  }

  static void navigateToOtp(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Otp()));
  }

  static void navigateToDashboard(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
  }

  static void navigateToEnterBuzz(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => EnterBuzz()));
  }

  static void navigateToCaptureFromURL(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CaptureFromURL()));
  }

  static void navigateToCyberAlerts(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CyberNews()));
  }

  static void navigateToAddParticipants(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => AddParticipants()));
  }

  static void navigateToNewDiscussion(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => NewDiscussion()));
  }

  static void navigateToCyberAdvisories(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CyberAdvisories()));
  }

  static void navigateToAddKnowledgeBase(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => AddKnowledgeBase()));
  }

  static void navigateToSearch(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Search()));
  }

  static void navigateToNotification(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => notification.Notification()));
  }

  static void navigateToNotificationDiscussion(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationDiscussion()));
  }

  static void navigateToNotificationDiscussionChat(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationDiscussionChat()));
  }

  static void navigateToNotificationDiscussionDetail(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationDiscussionDetail()));
  }

  static void navigateToNotificationDiscussionUpdate(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationDiscussionUpdate()));
  }

  static void navigateToProfile(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));
  }

  static void navigateToEditProfile(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile()));
  }

  static void navigateToMyActivity(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyActivity()));
  }

  static void navigateToMyFavorites(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyFavorites()));
  }

  static void navigateToMyFavoritesDiscussionChat(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyFavoriteDiscussionChat()));
  }

  static void navigateToMyFavoritesDiscussionDetail(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyFavoriteDiscussionDetail()));
  }

  static void navigateToViewKnowledgeBase(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ViewKnowledgeBase()));
  }

  static void navigateToHelpAndFeedback(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => HelpAndFeedback()));
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings}) : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
