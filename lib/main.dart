import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/view/splash.dart';

import 'routers/navigation_controller.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(statusBarColor: colors.Colors.redDark, statusBarBrightness: Brightness.light));
    return MaterialApp(
        debugShowCheckedModeBanner: false, theme: ThemeData(primarySwatch: colors.Colors.primary), home: Splash(), routes: NavigationController.configureRoutes());
  }
}
