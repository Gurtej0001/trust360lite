import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class HelpAndFeedback extends StatefulWidget {
  @override
  _HelpAndFeedbackState createState() => _HelpAndFeedbackState();
}

class _HelpAndFeedbackState extends State<HelpAndFeedback> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_left, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            backgroundColor: colors.Colors.redDark,
            title: Text("Help & Feedback", style: TextStyle(fontSize: 18))),
        body: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50, top: 50, bottom: 10),
                        child: Text(
                            "We are always looking to make TRUSTLite better for our members. Your valuable feedback can help us make this happen.",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50, top: 20, bottom: 0),
                        decoration: BoxDecoration(color: colors.Colors.greyLight),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: TextField(
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: Constants.subject,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                              border: InputBorder.none,
                            ),
                            cursorColor: Colors.red,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50, top: 20, bottom: 0),
                        decoration: BoxDecoration(color: colors.Colors.greyLight),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            minLines: 8,
                            decoration: InputDecoration(
                              hintText: Constants.description,
                              hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                              border: InputBorder.none,
                            ),
                            cursorColor: Colors.red,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: colors.Colors.redDark,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("Submit", style: TextStyle(color: Colors.white, fontSize: 18)),
                                ),
                              ],
                            ),
                          ),
                        ),
                        VerticalDivider(
                          width: 1,
                          color: Colors.grey,
                        ),
                        Expanded(
                          child: Container(
                            color: colors.Colors.redDark,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("Cancel", style: TextStyle(color: Colors.white, fontSize: 18)),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )));
  }
}
