import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class Otp extends StatefulWidget {
  @override
  _OtpState createState() => _OtpState();
}

class _OtpState extends State<Otp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 15,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('images/splash_logo.png'),
                      Container(
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 40, bottom: 0),
                        decoration:
                            BoxDecoration(color: colors.Colors.greyLight),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, top: 15, bottom: 15),
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.grey,
                                )),
                            Expanded(
                                flex: 1,
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  decoration: InputDecoration(
                                    hintText: Constants.sms_otp,
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 20),
                                    hintStyle: TextStyle(
                                        color: Colors.grey, fontSize: 18),
                                    border: InputBorder.none,
                                  ),
                                  cursorColor: Colors.red,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 20, bottom: 0),
                        decoration:
                            BoxDecoration(color: colors.Colors.greyLight),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, top: 15, bottom: 15),
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.grey,
                                )),
                            Expanded(
                                flex: 1,
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  decoration: InputDecoration(
                                    hintText: Constants.email_otp,
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 20),
                                    hintStyle: TextStyle(
                                        color: Colors.grey, fontSize: 18),
                                    border: InputBorder.none,
                                  ),
                                  cursorColor: Colors.red,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 20, bottom: 0),
                        child: Text(
                          Constants.resend_otp,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              color: colors.Colors.redDark,
                              fontSize: 18.0,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 40, bottom: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: ButtonTheme(
                                height: 50.0,
                                child: RaisedButton(
                                  onPressed: () =>
                                      NavigationController.navigateToDashboard(
                                          context),
                                  child: Text(Constants.verify_otp,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      )),
                                  color: colors.Colors.redDark,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Powered by",
                        style: TextStyle(
                          color: colors.Colors.redDark,
                          fontSize: 18,
                        )),
                    SizedBox(
                      width: 100,
                      height: 50,
                      child: Image.asset('images/ctm_logo.png'),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
