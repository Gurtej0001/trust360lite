import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 15,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('images/splash_logo.png'),
                      Container(
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 40, bottom: 0),
                        decoration:
                            BoxDecoration(color: colors.Colors.greyLight),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, right: 10, top: 15, bottom: 15),
                                child: Icon(
                                  Icons.mail_outline,
                                  color: Colors.grey,
                                )),
                            Expanded(
                                flex: 1,
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: Constants.username +
                                        " / " +
                                        Constants.email_id,
                                    labelStyle: TextStyle(
                                        color: Colors.grey, fontSize: 20),
                                    hintStyle: TextStyle(
                                        color: Colors.grey, fontSize: 18),
                                    border: InputBorder.none,
                                  ),
                                  cursorColor: Colors.red,
                                )),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 50, right: 50, top: 40, bottom: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: ButtonTheme(
                                height: 50.0,
                                child: RaisedButton(
                                  onPressed: () =>
                                      NavigationController.navigateToOtp(
                                          context),
                                  child: Text(Constants.sign_in,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                      )),
                                  color: colors.Colors.redDark,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Powered by",
                        style: TextStyle(
                          color: colors.Colors.redDark,
                          fontSize: 18,
                        )),
                    SizedBox(
                      width: 100,
                      height: 50,
                      child: Image.asset('images/ctm_logo.png'),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
