import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class NotificationDiscussionChat extends StatefulWidget {
  @override
  _NotificationDiscussionChatState createState() => _NotificationDiscussionChatState();
}

class _NotificationDiscussionChatState extends State<NotificationDiscussionChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.Colors.greyLight,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: RichText(
                text: TextSpan(children: [
              TextSpan(text: "New Discussion: new", style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
              TextSpan(text: "\n"),
              WidgetSpan(
                  child: Container(padding: EdgeInsets.only(top: 5.0), child: Text("Kunal appgenesis", style: TextStyle(fontSize: 14, color: Colors.white70))))
            ])),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.remove_red_eye, color: Colors.white), onPressed: () => NavigationController.navigateToNotificationDiscussionDetail(context))
            ]),
        body: Container(
            child: Stack(children: <Widget>[
          Center(child: Text("No Comment found", style: TextStyle(color: Colors.black54, fontSize: 18, fontWeight: FontWeight.bold))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                    IconButton(icon: new Icon(Icons.attach_file, color: Colors.black), onPressed: () => {}),
                    Expanded(
                        flex: 1,
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            decoration:
                                InputDecoration(hintText: "Type Comment...", hintStyle: TextStyle(color: Colors.grey, fontSize: 16), border: InputBorder.none),
                            cursorColor: Colors.red)),
                    Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
                        child: RawMaterialButton(
                            child: SizedBox(width: 40, height: 40, child: IconButton(icon: new Icon(Icons.send, color: Colors.white), onPressed: () => {})),
                            shape: CircleBorder(),
                            elevation: 2.0,
                            fillColor: colors.Colors.redDark,
                            onPressed: () {}))
                  ])))
        ])));
  }
}
