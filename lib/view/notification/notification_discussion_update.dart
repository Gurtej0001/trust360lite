import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class NotificationDiscussionUpdate extends StatefulWidget {
  @override
  _NotificationDiscussionUpdateState createState() => _NotificationDiscussionUpdateState();
}

class _NotificationDiscussionUpdateState extends State<NotificationDiscussionUpdate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Expanded(flex: 1, child: Text("UPDATE DISCUSSION", style: TextStyle(fontSize: 18))),
              Text("NEXT", style: TextStyle(fontSize: 18))
            ])),
        body: Column(children: <Widget>[
          Container(
              color: colors.Colors.greyLight,
              child: Column(children: <Widget>[
                Row(children: <Widget>[
                  IconButton(icon: Icon(Icons.people, color: Colors.red), onPressed: () => {}),
                  Flexible(
                      child: Container(
                          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                        child: Column(children: <Widget>[
                          Row(children: <Widget>[
                            Flexible(
                                child: TextField(
                                    controller: TextEditingController()..text = 'Abcd',
                                    cursorColor: Colors.black,
                                    maxLines: 1,
                                    style: TextStyle(color: Colors.black, fontSize: 14)))
                          ]),
                          Padding(
                              padding: EdgeInsets.only(top: 5.0),
                              child: Row(children: <Widget>[
                                Flexible(
                                    child: TextField(
                                        cursorColor: Colors.black,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: null,
                                        decoration: InputDecoration(hintText: "Description (Optional)", hintStyle: TextStyle(color: Colors.grey, fontSize: 14))))
                              ]))
                        ]))
                  ])))
                ])
              ]))
        ]));
  }
}
