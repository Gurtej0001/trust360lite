import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';

class NotificationDiscussion extends StatefulWidget {
  @override
  _NotificationDiscussionState createState() => _NotificationDiscussionState();
}

class _NotificationDiscussionState extends State<NotificationDiscussion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
            children: ListTile.divideTiles(context: context, tiles: [
      ListTile(
          onTap: () => NavigationController.navigateToNotificationDiscussionChat(context),
          title: Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: Column(children: <Widget>[
                Row(children: <Widget>[Text('New Discussion: new', style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))]),
                Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Row(children: <Widget>[
                      Text('#discussion', style: TextStyle(fontSize: 14.0, color: Colors.red)),
                      Text('  |  13 days ago', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                    ]))
              ]))),
      ListTile(
          onTap: () => NavigationController.navigateToNotificationDiscussionChat(context),
          title: Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: Column(children: <Widget>[
                Row(children: <Widget>[Text('New Discussion: abc', style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold))]),
                Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Row(children: <Widget>[
                      Text('#discussion', style: TextStyle(fontSize: 14.0, color: Colors.red)),
                      Text('  |  16 days ago', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                    ]))
              ])))
    ]).toList()));
  }
}
