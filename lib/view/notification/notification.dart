import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/view/notification/notification_discussion.dart';

class Notification extends StatefulWidget {
  @override
  _NotificationState createState() => _NotificationState();
}

class _NotificationState extends State<Notification> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
            appBar: AppBar(
                leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
                backgroundColor: colors.Colors.redDark,
                title: Text("Notification", style: TextStyle(fontSize: 20)),
                bottom: TabBar(
                    isScrollable: true,
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                    tabs: [Tab(text: "CURRENT BUZZ"), Tab(text: "DISCUSSION"), Tab(text: "KNOWLEDGE BASE")])),
            body: TabBarView(children: [
//              Center(child: Text("NO DATA FOUND", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black54))),
              Container(
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                      children: <Widget>[
                        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child:
                                        Text('Buzz testing', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberAdvisories', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ])
                      ]
                  )
              ),
              NotificationDiscussion(),
              Container(
                  color: Colors.white,
                  child: Column(children: <Widget>[
                    Row(children: <Widget>[
                      Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                      Flexible(
                          child: Container(
                              child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 2.0),
                                    child: Column(children: <Widget>[
                                      Row(children: <Widget>[
                                        Expanded(
                                            flex: 1,
                                            child: Text("Micorsoft Releases Patches for 64 Flaws", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                      ]),
                                      Padding(
                                          padding: EdgeInsets.only(top: 5.0),
                                          child: Row(children: <Widget>[
                                            Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                            Text(
                                                '  |  rehab, 9 months ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                          ]))
                                    ]))
                              ])))
                    ])
                  ]))
            ])));
  }
}
