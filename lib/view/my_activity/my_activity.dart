import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class MyActivity extends StatefulWidget {
  @override
  _MyActivityState createState() => _MyActivityState();
}

class _MyActivityState extends State<MyActivity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("My Activity", style: TextStyle(fontSize: 18))),
        /*body: Container(
            color: Colors.white,
            child: Column(children: <Widget>[
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("CURRENT BUZZ", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ])),
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("DISCUSSION", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ])),
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("KNOWLEDGE BASE", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ]))
            ])));*/
        body: Container(
            color: Colors.white,
            child: Column(children: <Widget>[
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("CURRENT BUZZ", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ])),
              Container(
                  margin: EdgeInsets.all(10.0),
                  child: InkWell(
                      onTap: () => NavigationController.navigateToCyberAlerts(context),
                      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                        Column(children: <Widget>[
                          Stack(children: <Widget>[
                            SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                            Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star, size: 25.0, color: colors.Colors.redDark))
                          ])
                        ]),
                        Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: Container(
                                height: 60,
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                  Expanded(
                                      child: Text('Test with app new design',
                                          maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                  Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Text('#CyberAlerts', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                    Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                    Text('Posted 4 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                  ])
                                ])))
                      ]))),
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("DISCUSSION", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ])),
              InkWell(
                  onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                  child: Container(
                      color: Colors.white,
                      child: Row(children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(bottom: 40),
                            width: 30,
                            height: 30,
                            child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 26.0), onPressed: () => {})),
                        Container(
                            height: 60,
                            width: 60,
                            child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 10,
                                child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                            decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                        Flexible(
                            child: Container(
                                child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(10.0),
                                      child: Column(children: <Widget>[
                                        Row(children: <Widget>[
                                          Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                          Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                          Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                          Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                          Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                        ]),
                                        Padding(
                                            padding: EdgeInsets.only(top: 20.0),
                                            child: Row(children: <Widget>[
                                              Flexible(
                                                  child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                              Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                            ]))
                                      ]))
                                ])))
                      ]))),
              Container(
                  color: Colors.grey,
                  child: Row(children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[Text("KNOWLEDGE BASE", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold))]))
                  ])),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
//                          IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 30), onPressed: () => {}),
//                          IconButton(icon: Icon(Icons.insert_drive_file, color: Colors.red, size: 36), onPressed: () => {}),
//                          Image.asset('images/document.png', fit: BoxFit.fill, width: 50, height: 50),
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 2.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("Micorsoft Releases Patches for 64 Flaws", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.red, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text('  |  rehab, 9 months ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1, child: Text("W97M?Downloader and X97M/Downloader", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.red, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text('  |  wafa, over a year ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey)
            ])));
  }
}
