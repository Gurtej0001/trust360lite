import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:speech_bubble/speech_bubble.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class CyberNews extends StatefulWidget {
  @override
  _CyberNewsState createState() => _CyberNewsState();
}

class _CyberNewsState extends State<CyberNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.Colors.greyLight,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("Cyber News", style: TextStyle(fontSize: 18))),
        body: Column(children: <Widget>[
          Expanded(
              flex: 1,
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
                Container(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  Container(
                      color: Colors.white,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                        Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Stack(children: <Widget>[
                            SizedBox(width: double.infinity, height: 200, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                            Positioned(top: 5.0, left: 5.0, child: Icon(Icons.star_border, size: 30.0, color: Colors.grey))
                          ]),
                          Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                  height: 75,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child: Text('New SIM Card Flaw Lets Hackers Hijack Any Phone Just By Sending',
                                            style: TextStyle(fontSize: 20.0, color: Colors.black))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('Cyber News', style: TextStyle(fontSize: 16.0, color: Colors.black)),
                                      Text(' 3 months ago, zubair', style: TextStyle(fontSize: 16.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]),
                        Container(
                            margin: EdgeInsets.only(top: 15, bottom: 15),
                            child: Padding(
                                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                                child: Text(
                                    "Cyberscurity researchers today revelead the exixtence of Simjacker, a SIM Card vulnerability that could allow remote hackers to spy on devices just by sendind SMS",
                                    style: TextStyle(color: Colors.black54, fontSize: 16.0)))),
                        Padding(padding: const EdgeInsets.all(10.0), child: Text("Source", style: TextStyle(color: colors.Colors.redDark)))
                      ])),
                  Container(
                      color: colors.Colors.redDark,
                      width: double.infinity,
                      child: Padding(padding: const EdgeInsets.all(10.0), child: Text("3 Comments", style: TextStyle(color: Colors.white)))),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.65,
                      margin: EdgeInsets.only(left: 20, top: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                      child: SpeechBubble(
                          color: Colors.white,
                          nipLocation: NipLocation.LEFT,
                          nipHeight: 15.0,
                          child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                        Text("Zubair", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                        Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                        Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                      ])))),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.65,
                      margin: EdgeInsets.only(left: 20, top: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                      child: SpeechBubble(
                          color: Colors.white,
                          nipLocation: NipLocation.LEFT,
                          nipHeight: 15.0,
                          child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                        Text("raghwendra", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                        Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Test", style: TextStyle(color: Colors.black54, fontSize: 14))),
                        Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                      ])))),
                      Container(
                          width: MediaQuery.of(context).size.width * 0.65,
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.30, top: 10),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                          child: SpeechBubble(
                              color: Colors.grey,
                              nipLocation: NipLocation.RIGHT,
                              nipHeight: 15.0,
                              child: Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Text("katelin.funk", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                                    Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                                    Padding(
                                        padding: const EdgeInsets.only(top: 5.0),
                                        child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                                  ])))),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.65,
                        margin: EdgeInsets.only(left: 20, top: 10),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                        child: SpeechBubble(
                            color: Colors.white,
                            nipLocation: NipLocation.LEFT,
                            nipHeight: 15.0,
                            child: Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("raghwendra", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                          Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("There is no comment", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("6 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
                  )
                ]))
              ]))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                    IconButton(icon: new Icon(Icons.attach_file, color: Colors.black), onPressed: () => {}),
                    Expanded(
                        flex: 1,
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            decoration: InputDecoration(hintText: "Type Comment...", hintStyle: TextStyle(color: Colors.grey, fontSize: 16), border: InputBorder.none),
                            cursorColor: Colors.red)),
                    Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
                        child: RawMaterialButton(
                            child: SizedBox(width: 40, height: 40, child: IconButton(icon: new Icon(Icons.send, color: Colors.white), onPressed: () => {})),
                            shape: CircleBorder(),
                            elevation: 2.0,
                            fillColor: colors.Colors.redDark,
                            onPressed: () {}))
                  ])))
        ]));
  }
}
