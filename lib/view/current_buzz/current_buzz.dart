import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class CurrentBuzz extends StatefulWidget {
  @override
  _CurrentBuzzState createState() => _CurrentBuzzState();
}

class _CurrentBuzzState extends State<CurrentBuzz> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: ListView(
              shrinkWrap: true,
              children: ListTile.divideTiles(context: context, tiles: [
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Stack(children: <Widget>[
                            SizedBox(width: double.infinity, height: 200, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                            Positioned(top: 5.0, left: 5.0, child: Icon(Icons.star_border, size: 30.0, color: Colors.grey))
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child: Text('New SIM Card Flaw Lets Hackers Hijack Any Phone Just By Sending',
                                            style: TextStyle(fontSize: 16.0, color: colors.Colors.redDark, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('Cyber News', style: TextStyle(fontSize: 14.0, color: colors.Colors.redDark)),
                                      Text(' 3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star, size: 25.0, color: colors.Colors.redDark))
                            ])
                          ]),
                          Flexible(
                            child: Container(
                                height: 60,
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start,children: <Widget>[
                                  Expanded(child: Text('Test with app new design', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                  Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Text('#CyberAlerts', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                    Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                    Expanded(flex: 1,child: Text('  |  3 months ago, zubair', overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14.0, color: Colors.grey))),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 2.0),
                                      child: Container(
                                          width: 22,
                                          height: 22,
                                          child: Center(child: Text("2", style: TextStyle(color: Colors.white))),
                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green)))
                                  ])
                                ])),
                          )
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Flexible(
                              child: Container(
                                  height: 60,
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Text('New Trickbot variant found',
                                        maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold)),
                                    Text('highly obfuscated JS file..',
                                        maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold)),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberAdvisories', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Expanded(flex: 1,child: Text('7 months ago raghwendra', overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 12.0, color: Colors.grey))),
                                      Padding(
                                          padding: const EdgeInsets.only(left: 2.0),
                                          child: Container(
                                              width: 22,
                                              height: 22,
                                              child: Center(child: Text("10", style: TextStyle(color: Colors.white))),
                                              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green)))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child:
                                            Text('Buzz testing', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberAdvisories', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child:
                                            Text('testing title', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberAdvisories', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child: Text('title789', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberAdvisories', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(
                                        child: Text('test0987', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberReports', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(child: Text('Latest', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberReports', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(child: Text('Latest', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberReports', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ]))),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: InkWell(
                        onTap: () => NavigationController.navigateToCyberAlerts(context),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Column(children: <Widget>[
                            Stack(children: <Widget>[
                              SizedBox(width: 80, height: 50, child: Image.asset('images/demo_image.jpg', fit: BoxFit.fill)),
                              Positioned(top: 2.0, left: 2.0, child: Icon(Icons.star_border, size: 25.0, color: Colors.grey))
                            ])
                          ]),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  height: 60,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                    Expanded(child: Text('Latest', maxLines: 2, style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold))),
                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                      Text('#CyberReports', style: TextStyle(fontSize: 12.0, color: Colors.red)),
                                      Container(width: 1.0, height: 10.0, color: Colors.grey, margin: EdgeInsets.only(left: 5, right: 5)),
                                      Text('7 months ago raghwendra', style: TextStyle(fontSize: 12.0, color: Colors.grey))
                                    ])
                                  ])))
                        ])))
              ]).toList()),
        ),
        floatingActionButton: SpeedDial(
            marginRight: 16,
            marginBottom: 16,
            animatedIcon: AnimatedIcons.menu_close,
            animatedIconTheme: IconThemeData(size: 22.0),
            visible: true,
            closeManually: false,
            curve: Curves.bounceIn,
            overlayColor: Colors.black,
            overlayOpacity: 0.5,
            backgroundColor: colors.Colors.redDark,
            foregroundColor: Colors.white,
            elevation: 8.0,
            shape: CircleBorder(),
            children: [
              SpeedDialChild(
                  child: Icon(Icons.add, color: Colors.white),
                  backgroundColor: colors.Colors.redDark,
                  label: 'Enter Buzz',
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () => NavigationController.navigateToEnterBuzz(context)),
              SpeedDialChild(
                  child: Icon(Icons.content_copy, color: Colors.white),
                  backgroundColor: colors.Colors.redDark,
                  label: 'Capture from URL',
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () => NavigationController.navigateToCaptureFromURL(context))
            ]));
  }
}
