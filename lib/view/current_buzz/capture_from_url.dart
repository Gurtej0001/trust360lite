import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class CaptureFromURL extends StatefulWidget {
  @override
  _CaptureFromURLState createState() => _CaptureFromURLState();
}

class _CaptureFromURLState extends State<CaptureFromURL> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("Add Buzz", style: TextStyle(fontSize: 18))),
        body: Column(children: <Widget>[
          Expanded(
              flex: 1,
              child: SingleChildScrollView(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    child: Row(children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Container(
                              decoration: BoxDecoration(color: colors.Colors.greyLight),
                              child: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: TextField(
                                      keyboardType: TextInputType.text,
                                      decoration:
                                          InputDecoration(hintText: "Enter URL", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                                      cursorColor: Colors.red)))),
                      Container(
                          margin: EdgeInsets.all(10.0),
                          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
                          color: colors.Colors.redDark,
                          child: Text("GET", style: TextStyle(color: Colors.white, fontSize: 18)))
                    ])),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Buzz Category",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(children: <Widget>[
                          Icon(Icons.camera_alt, color: Colors.grey),
                          Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Container(
                                  padding: EdgeInsets.all(5.0),
                                  color: colors.Colors.redDark,
                                  child: Text("Choose image", style: TextStyle(color: Colors.white, fontSize: 16)))),
                          Padding(padding: const EdgeInsets.only(left: 10.0), child: Text("Select Images", style: TextStyle(color: Colors.grey, fontSize: 16)))
                        ]))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Type",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Country",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Region",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(hintText: "Enter Title", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            minLines: 8,
                            decoration:
                                InputDecoration(hintText: Constants.description, hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                            cursorColor: Colors.red)))
              ]))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                    Expanded(
                        child: Container(
                            color: colors.Colors.redDark,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                              Padding(padding: const EdgeInsets.all(10.0), child: Text("Preview", style: TextStyle(color: Colors.white, fontSize: 18)))
                            ]))),
                    VerticalDivider(width: 1, color: Colors.grey),
                    Expanded(
                        child: Container(
                            color: colors.Colors.redDark,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                              Padding(padding: const EdgeInsets.all(10.0), child: Text("Cancel", style: TextStyle(color: Colors.white, fontSize: 18)))
                            ])))
                  ])))
        ]));
  }
}
