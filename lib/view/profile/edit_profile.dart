import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("Edit Profile", style: TextStyle(fontSize: 20))),
        body: Stack(fit: StackFit.expand, children: <Widget>[
          Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 40),
                child: SizedBox(height: 100.0, width: 100.0, child: CircleAvatar(radius: 0, backgroundImage: AssetImage('images/profile.png')))),
            Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 40, bottom: 0),
                decoration: BoxDecoration(color: colors.Colors.greyLight),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Padding(padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15), child: Icon(Icons.account_circle, color: Colors.grey)),
                  Expanded(
                      flex: 1,
                      child: TextField(
                          decoration: InputDecoration(hintText: "katelin.funk", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                          cursorColor: Colors.red))
                ])),
            Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 20, bottom: 0),
                decoration: BoxDecoration(color: colors.Colors.greyLight),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Padding(padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15), child: Icon(Icons.mobile_screen_share, color: Colors.grey)),
                  Expanded(
                      flex: 1,
                      child: TextField(
                          decoration: InputDecoration(hintText: "0091984123456", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                          cursorColor: Colors.red))
                ])),
            Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 20, bottom: 0),
                decoration: BoxDecoration(color: colors.Colors.greyLight),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Padding(padding: const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 15), child: Icon(Icons.location_on, color: Colors.grey)),
                  Expanded(flex: 1, child: TextField(decoration: InputDecoration(border: InputBorder.none), cursorColor: Colors.red))
                ])),
            Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 60, bottom: 0),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: ButtonTheme(
                          height: 50.0,
                          child: RaisedButton(
                              onPressed: () => {},
                              child: Text(Constants.submit, style: TextStyle(fontSize: 18, color: Colors.white)),
                              color: colors.Colors.redDark)))
                ]))
          ])
        ]));
  }
}
