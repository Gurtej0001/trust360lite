import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  double _containerHeight = 150, _imageHeight = 70, _iconLeft = 12, _marginLeft = 90;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
            child: Column(children: <Widget>[
          Container(
              height: 200,
              color: Colors.white,
              child: Stack(children: <Widget>[
                Positioned(left: 0, right: 0, height: _containerHeight, child: Container(color: colors.Colors.redDark)),
                Positioned(
                    top: 20.0,
                    child: Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
                          Text('My Profile', style: TextStyle(fontSize: 20.0, color: Colors.white))
                        ]))),
                Positioned(
                    left: _iconLeft,
                    top: _containerHeight - _imageHeight / 2,
                    child: Container(
                        height: _imageHeight,
                        width: _imageHeight,
                        child: Padding(
                            padding: EdgeInsets.all(5),
                            child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 10,
                                child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: _imageHeight, width: _imageHeight)))),
                        decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0)))),
                Positioned(
                    left: _marginLeft,
                    top: _containerHeight - (_imageHeight / 2.5),
                    child: Row(children: <Widget>[Text("AG Kunal", style: TextStyle(color: Colors.white, fontSize: 18))])),
                Positioned(
                    left: MediaQuery.of(context).size.width * 0.88,
                    top: _containerHeight - (_imageHeight / 2),
                    child: Row(children: <Widget>[
                      IconButton(icon: Icon(Icons.edit, color: Colors.white, size: 16), onPressed: () => NavigationController.navigateToEditProfile(context))
                    ]))
              ])),
          Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[Text('Full Name', style: TextStyle(fontSize: 12.0, color: Colors.grey))])
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Flexible(
                              child: TextField(
                                  enabled: false,
                                  decoration: const InputDecoration(hintText: "AG Kunal", hintStyle: TextStyle(fontSize: 16.0, color: Colors.black))))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 10.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[Text('Alias Name', style: TextStyle(fontSize: 12.0, color: Colors.grey))])
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Flexible(
                              child: TextField(
                                  enabled: false,
                                  decoration: const InputDecoration(hintText: "Katelin.funk", hintStyle: TextStyle(fontSize: 16.0, color: Colors.black))))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[Text('Email Address', style: TextStyle(fontSize: 12.0, color: Colors.grey))])
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Flexible(
                              child: TextField(
                                  enabled: false,
                                  decoration:
                                      const InputDecoration(hintText: "Kunal.appgenesis@gmail.com", hintStyle: TextStyle(fontSize: 16.0, color: Colors.black))))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[Text('Address', style: TextStyle(fontSize: 12.0, color: Colors.grey))])
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Flexible(
                              child: TextField(
                                  enabled: false, decoration: const InputDecoration(hintText: "", hintStyle: TextStyle(fontSize: 16.0, color: Colors.black))))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                        child: Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Expanded(flex: 2, child: Container(child: Text('Country', style: TextStyle(fontSize: 12.0, color: Colors.grey)))),
                          Expanded(flex: 2, child: Container(child: Text('City', style: TextStyle(fontSize: 12.0, color: Colors.grey))))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Flexible(
                              flex: 2,
                              child:
                                  Padding(padding: EdgeInsets.only(right: 15.0), child: TextField(enabled: false, decoration: const InputDecoration(hintText: "")))),
                          Flexible(flex: 2, child: TextField(enabled: false, decoration: const InputDecoration(hintText: "")))
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[Text('Mobile No', style: TextStyle(fontSize: 12.0, color: Colors.grey))])
                        ])),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                          Flexible(
                              child: TextField(
                                  enabled: false,
                                  decoration: const InputDecoration(hintText: "0091984123456", hintStyle: TextStyle(fontSize: 16.0, color: Colors.black))))
                        ]))
                  ])))
        ])));
  }
}
