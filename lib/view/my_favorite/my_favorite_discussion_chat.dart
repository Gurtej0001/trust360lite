import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:speech_bubble/speech_bubble.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class MyFavoriteDiscussionChat extends StatefulWidget {
  @override
  _MyFavoriteDiscussionChatState createState() => _MyFavoriteDiscussionChatState();
}

class _MyFavoriteDiscussionChatState extends State<MyFavoriteDiscussionChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.Colors.greyLight,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: RichText(
                text: TextSpan(children: [
              TextSpan(text: "Private Chat with Raghu", style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold)),
              TextSpan(text: "\n"),
              WidgetSpan(child: Container(padding: EdgeInsets.only(top: 5.0), child: Text("zubair", style: TextStyle(fontSize: 14, color: Colors.white70))))
            ])),
            actions: <Widget>[
              IconButton(icon: Icon(Icons.remove_red_eye, color: Colors.white), onPressed: () => NavigationController.navigateToMyFavoritesDiscussionDetail(context))
            ]),
        body: Container(
            child: Stack(children: <Widget>[
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: 20, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.white,
                    nipLocation: NipLocation.LEFT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("zubair", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                          Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Hi Raghu..", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: 20, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.white,
                    nipLocation: NipLocation.LEFT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("zbair", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                          Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.30, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.grey,
                    nipLocation: NipLocation.RIGHT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("katelin.funk", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                          Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: 20, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.white,
                    nipLocation: NipLocation.LEFT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("zubair", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                          Padding(
                              padding: const EdgeInsets.only(top: 2.0),
                              child: Text("No.. notifications for chat", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: 20, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.white,
                    nipLocation: NipLocation.LEFT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("zubair", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                          Padding(
                              padding: const EdgeInsets.only(top: 2.0),
                              child: Text("Raghu.. can u comment here..", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ])))),
            Container(
                width: MediaQuery.of(context).size.width * 0.65,
                margin: EdgeInsets.only(left: 20, top: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                child: SpeechBubble(
                    color: Colors.white,
                    nipLocation: NipLocation.LEFT,
                    nipHeight: 15.0,
                    child: Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                          Text("raghwendra", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold)),
                          Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Hi", style: TextStyle(color: Colors.black54, fontSize: 14))),
                          Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Align(alignment: Alignment.centerRight, child: Text("3 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                        ]))))
          ]),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                    IconButton(icon: new Icon(Icons.attach_file, color: Colors.black), onPressed: () => {}),
                    Expanded(
                        flex: 1,
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            decoration:
                                InputDecoration(hintText: "Type Comment...", hintStyle: TextStyle(color: Colors.grey, fontSize: 16), border: InputBorder.none),
                            cursorColor: Colors.red)),
                    Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
                        child: RawMaterialButton(
                            child: SizedBox(width: 40, height: 40, child: IconButton(icon: new Icon(Icons.send, color: Colors.white), onPressed: () => {})),
                            shape: CircleBorder(),
                            elevation: 2.0,
                            fillColor: colors.Colors.redDark,
                            onPressed: () {}))
                  ])))
        ])));
  }
}
