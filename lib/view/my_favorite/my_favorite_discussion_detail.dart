import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class MyFavoriteDiscussionDetail extends StatefulWidget {
  @override
  _MyFavoriteDiscussionDetailState createState() => _MyFavoriteDiscussionDetailState();
}

class _MyFavoriteDiscussionDetailState extends State<MyFavoriteDiscussionDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("DISCUSSION DETAIL", style: TextStyle(fontSize: 20))),
        body: Stack(fit: StackFit.expand, children: <Widget>[
          Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Container(
                child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Container(margin: EdgeInsets.only(top: 25, bottom: 25), child: Padding(padding: EdgeInsets.all(30), child: Image.asset('images/ctm_logo.png'))),
              Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Text("Private Chat with Raghu",
                                  textAlign: TextAlign.left, style: TextStyle(fontWeight: FontWeight.bold, color: colors.Colors.redDark, fontSize: 16.0)))
                        ]),
                        Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Column(children: <Widget>[
                              Row(children: <Widget>[Text('Add Description', style: TextStyle(fontSize: 14.0, color: Colors.black))]),
                              Padding(
                                  padding: EdgeInsets.only(top: 5.0),
                                  child: Row(children: <Widget>[Text('Created by zubair 3 months ago', style: TextStyle(fontSize: 14.0, color: Colors.grey))]))
                            ]))
                      ])))
            ]))
          ])
        ]));
  }
}
