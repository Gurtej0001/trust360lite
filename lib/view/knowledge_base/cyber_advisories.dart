import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class CyberAdvisories extends StatefulWidget {
  @override
  _CyberAdvisoriesState createState() => _CyberAdvisoriesState();
}

class _CyberAdvisoriesState extends State<CyberAdvisories> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("Cyber Advisories", style: TextStyle(fontSize: 18))),
        body: Container(
            color: Colors.white,
            child: Column(children: <Widget>[
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 2.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("Micorsoft Releases Patches for 64 Flaws", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.grey, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text(
                                                    '  |  rehab, 9 months ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(
                  color: Colors.grey
              ),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("W97M?Downloader and X97M/Downloader", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.grey, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text('  |  wafa, over a year ago',
                                                    style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 2.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("Micorsoft Releases Patches for 64 Flaws", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.grey, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text(
                                                    '  |  rehab, 9 months ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("W97M?Downloader and X97M/Downloader", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.grey, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text('  |  wafa, over a year ago',
                                                    style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 2.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("Micorsoft Releases Patches for 64 Flaws", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.grey, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Flexible(child: Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12))),
                                                Text(
                                                    '  |  rehab, 9 months ago', style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey),
              InkWell(
                  onTap: () => NavigationController.navigateToViewKnowledgeBase(context),
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 5, bottom: 40),
                              width: 25,
                              height: 25,
                              child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 25.0), onPressed: () => {})),
                          Image.asset("images/document.png", fit: BoxFit.cover, height: 60, width: 60),
                          Flexible(
                              child: Container(
                                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                                        child: Column(children: <Widget>[
                                          Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Text("W97M?Downloader and X97M/Downloader", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                            Icon(Icons.file_download, color: Colors.red, size: 18)
                                          ]),
                                          Padding(
                                              padding: EdgeInsets.only(top: 5.0),
                                              child: Row(children: <Widget>[
                                                Text("", style: TextStyle(color: colors.Colors.redDark, fontSize: 12)),
                                                Expanded(flex: 1, child: Text('  |  wafa, over a year ago',
                                                    style: TextStyle(fontSize: 12.0, color: Colors.black54, fontWeight: FontWeight.bold))),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 10.0),
                                                  child: Container(
                                                      width: 20,
                                                      height: 20,
                                                      child: Center(child: Text("2", style: TextStyle(color: Colors.white))),
                                                      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green)),
                                                )
                                              ]))
                                        ]))
                                  ])))
                        ])
                      ]))),
              Divider(color: Colors.grey)
            ])),
        floatingActionButton: FloatingActionButton(child: Icon(Icons.add), onPressed: () => NavigationController.navigateToAddKnowledgeBase(context)));
  }
}
