import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/view/knowledge_base/round_chart.dart';

class AddKnowledgeBase extends StatefulWidget {
  @override
  _AddKnowledgeBaseState createState() => _AddKnowledgeBaseState();
}

class _AddKnowledgeBaseState extends State<AddKnowledgeBase> {
  final data = [new LinearSales(0, 100, colors.Colors.redDark)];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("Add Knowledge Base".toUpperCase(), style: TextStyle(fontSize: 18))),
        body: Column(children: <Widget>[
          Expanded(
              flex: 1,
              child: SingleChildScrollView(
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Type",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Category",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Country",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                hintText: "Select Region",
                                hintStyle: TextStyle(color: Colors.grey, fontSize: 18),
                                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey),
                                border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration:
                                InputDecoration(hintText: "Title of Repory", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            minLines: 8,
                            decoration:
                                InputDecoration(hintText: "Enter Description", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                            cursorColor: Colors.red))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(children: <Widget>[
                          Icon(Icons.attach_file, color: Colors.grey),
                          Padding(padding: const EdgeInsets.only(left: 10.0), child: Text("Attach Report", style: TextStyle(color: Colors.grey, fontSize: 16)))
                        ]))),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                    decoration: BoxDecoration(color: colors.Colors.greyLight),
                    child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: TextField(
                            enabled: false,
                            keyboardType: TextInputType.text,
                            decoration:
                                InputDecoration(hintText: "Enter URL(if any)", hintStyle: TextStyle(color: Colors.grey, fontSize: 18), border: InputBorder.none),
                            cursorColor: Colors.red)))
              ]))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                    Expanded(
                        child: Container(
                            color: colors.Colors.redDark,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                              Padding(padding: const EdgeInsets.all(10.0), child: Text("Preview", style: TextStyle(color: Colors.white, fontSize: 18)))
                            ]))),
                    VerticalDivider(width: 1, color: Colors.grey),
                    Expanded(
                        child: Container(
                            color: colors.Colors.redDark,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                              Padding(padding: const EdgeInsets.all(10.0), child: Text("Cancel", style: TextStyle(color: Colors.white, fontSize: 18)))
                            ])))
                  ])))
        ]));
  }
}
