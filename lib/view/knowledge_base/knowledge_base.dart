import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/view/knowledge_base/round_chart.dart';

class KnowledgeBase extends StatefulWidget {
  @override
  _KnowledgeBaseState createState() => _KnowledgeBaseState();
}

class _KnowledgeBaseState extends State<KnowledgeBase> {
  final data = [new LinearSales(0, 100, colors.Colors.redDark)];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            InkWell(
                onTap: () => NavigationController.navigateToCyberAdvisories(context),
                child: Stack(children: <Widget>[
                  SizedBox(width: MediaQuery.of(context).size.width * 0.50, height: MediaQuery.of(context).size.width * 0.50, child: Charts.chartDesign(true, 1, data, 10)),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.50,
                      height: MediaQuery.of(context).size.width * 0.50,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Text("2", style: TextStyle(fontSize: 20, color: Colors.black)),
                        Text("IOCs", style: TextStyle(fontSize: 12, color: colors.Colors.redDark))
                      ]))
                ])),
            InkWell(
                onTap: () => NavigationController.navigateToCyberAdvisories(context),
                child: Stack(children: <Widget>[
                  SizedBox(width: MediaQuery.of(context).size.width * 0.50, height: MediaQuery.of(context).size.width * 0.50, child: Charts.chartDesign(true, 1, data, 10)),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.50,
                      height: MediaQuery.of(context).size.width * 0.50,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Text("17", style: TextStyle(fontSize: 20, color: Colors.black)),
                        Text("Cyber Reports", style: TextStyle(fontSize: 12, color: colors.Colors.redDark))
                      ]))
                ]))
          ]),
          Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            InkWell(
                onTap: () => NavigationController.navigateToCyberAdvisories(context),
                child: Stack(children: <Widget>[
                  SizedBox(width: MediaQuery.of(context).size.width * 0.50, height: MediaQuery.of(context).size.width * 0.50, child: Charts.chartDesign(true, 1, data, 10)),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.50,
                      height: MediaQuery.of(context).size.width * 0.50,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Text("20", style: TextStyle(fontSize: 20, color: Colors.black)),
                        Text("Cyber Advisories", style: TextStyle(fontSize: 12, color: colors.Colors.redDark))
                      ]))
                ])),
            InkWell(
                onTap: () => NavigationController.navigateToCyberAdvisories(context),
                child: Stack(children: <Widget>[
                  SizedBox(width: MediaQuery.of(context).size.width * 0.50, height: MediaQuery.of(context).size.width * 0.50, child: Charts.chartDesign(true, 1, data, 10)),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.50,
                      height: MediaQuery.of(context).size.width * 0.50,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Text("3", style: TextStyle(fontSize: 20, color: Colors.black)),
                        Text("General", style: TextStyle(fontSize: 12, color: colors.Colors.redDark))
                      ]))
                ]))
          ])
        ]),
        floatingActionButton: FloatingActionButton(child: Icon(Icons.add), onPressed: () => NavigationController.navigateToAddKnowledgeBase(context)));
  }
}
