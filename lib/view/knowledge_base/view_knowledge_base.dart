import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:speech_bubble/speech_bubble.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class ViewKnowledgeBase extends StatefulWidget {
  @override
  _ViewKnowledgeBaseState createState() => _ViewKnowledgeBaseState();
}

class _ViewKnowledgeBaseState extends State<ViewKnowledgeBase> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: colors.Colors.greyLight,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Text("View Knowledge Base".toUpperCase(), style: TextStyle(fontSize: 18))),
        body: Stack(children: <Widget>[
          SingleChildScrollView(
            child: Container(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Container(
                  color: Colors.white,
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start,children: <Widget>[
                        Row(children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                              child: Image.asset("images/document.png", fit: BoxFit.cover, height: 30, width: 30)),
                          Flexible(
                              child: Container(
                                  child: Padding(
                                      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Row(children: <Widget>[
                                          Text("Microsoft Releases Patches for..", style: TextStyle(color: colors.Colors.redDark, fontSize: 16))
                                        ]),
                                        Padding(
                                            padding: EdgeInsets.only(top: 5.0),
                                            child: Row(children: <Widget>[Text("IOCS", style: TextStyle(color: Colors.grey, fontSize: 14))]))
                                      ]))))
                        ]),
                        Container(
                            color: colors.Colors.greyLight,
                            width: double.infinity,
                            margin: EdgeInsets.only(top: 10),
                            child: Padding(padding: const EdgeInsets.all(5.0), child: Text("Others", style: TextStyle(color: colors.Colors.redDark)))),
                        Container(
                            margin: EdgeInsets.only(top: 15, bottom: 15),
                            child: Text("It's time for another batch of \"Patch Tuesday\" updates from Microsoft.", style: TextStyle(color: Colors.black54))),
                        Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(children: <Widget>[
                              Expanded(flex: 1, child: Text("Source", style: TextStyle(color: colors.Colors.redDark))),
                              Icon(Icons.file_download, color: Colors.red)
                            ]))
                      ]))),
              Container(
                  color: colors.Colors.redDark,
                  width: double.infinity,
                  child: Padding(padding: const EdgeInsets.only(left: 30.0, top: 5.0, bottom: 5.0), child: Text("3 Comments", style: TextStyle(color: Colors.white)))),
              Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  margin: EdgeInsets.only(left: 20, top: 10),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                  child: SpeechBubble(
                      color: Colors.white,
                      nipLocation: NipLocation.LEFT,
                      nipHeight: 15.0,
                      child: Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            Text("Zubair", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                            Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                            Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                          ])))),
              Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  margin: EdgeInsets.only(left: 20, top: 10),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                  child: SpeechBubble(
                      color: Colors.white,
                      nipLocation: NipLocation.LEFT,
                      nipHeight: 15.0,
                      child: Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            Text("raghwendra", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                            Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Test", style: TextStyle(color: Colors.black54, fontSize: 14))),
                            Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                          ])))),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.65,
                      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.30, top: 10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                      child: SpeechBubble(
                          color: Colors.grey,
                          nipLocation: NipLocation.RIGHT,
                          nipHeight: 15.0,
                          child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                Text("katelin.funk", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                                Padding(padding: const EdgeInsets.only(top: 2.0), child: Text("Hello", style: TextStyle(color: Colors.black54, fontSize: 14))),
                                Padding(
                                    padding: const EdgeInsets.only(top: 5.0),
                                    child: Align(alignment: Alignment.centerRight, child: Text("7 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                              ])))),
              Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  margin: EdgeInsets.only(left: 20, top: 10, bottom: 70),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.white),
                  child: SpeechBubble(
                      color: Colors.white,
                      nipLocation: NipLocation.LEFT,
                      nipHeight: 15.0,
                      child: Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            Text("raghwendra", style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold)),
                            Padding(
                                padding: const EdgeInsets.only(top: 2.0), child: Text("There is no comment", style: TextStyle(color: Colors.black54, fontSize: 14))),
                            Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Align(alignment: Alignment.centerRight, child: Text("6 months ago", style: TextStyle(color: Colors.black54, fontSize: 12))))
                          ]))))
            ])),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: Theme.of(context).highlightColor))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                    IconButton(icon: new Icon(Icons.attach_file, color: Colors.black), onPressed: () => {}),
                    Expanded(
                        flex: 1,
                        child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            decoration:
                                InputDecoration(hintText: "Type Comment...", hintStyle: TextStyle(color: Colors.grey, fontSize: 16), border: InputBorder.none),
                            cursorColor: Colors.red)),
                    Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
                        child: RawMaterialButton(
                            child: SizedBox(width: 40, height: 40, child: IconButton(icon: new Icon(Icons.send, color: Colors.white), onPressed: () => {})),
                            shape: CircleBorder(),
                            elevation: 2.0,
                            fillColor: colors.Colors.redDark,
                            onPressed: () {}))
                  ])))
        ]));
  }
}
