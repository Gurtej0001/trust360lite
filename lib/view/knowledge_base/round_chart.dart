import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Charts extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final int animationDuration;
  final int arcWidth;

  Charts(this.seriesList, {this.animate, this.animationDuration, this.arcWidth});

  /// Creates a [PieChart] with sample data and no transition.
  factory Charts.chartDesign(bool animate, int animationDuration, List graphChartValue, int arcWidth) {
    return new Charts(_createDashboardData(graphChartValue),
        // Disable animations for image tests.
        animate: animate,
        animationDuration: animationDuration,
        arcWidth: arcWidth);
  }

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate, animationDuration: Duration(seconds: animationDuration), defaultRenderer: new charts.ArcRendererConfig(arcWidth: arcWidth));
  }

  /// Create one series with sample hard coded data.

  static List<charts.Series<LinearSales, int>> _createDashboardData(List graphChartValue) {
    return [
      new charts.Series<LinearSales, int>(
          id: 'Sales',
          domainFn: (LinearSales sales, _) => sales.year,
          measureFn: (LinearSales sales, _) => sales.sales,
          colorFn: (LinearSales sales, _) => charts.ColorUtil.fromDartColor(sales.colorName),
          data: graphChartValue)
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;
  final Color colorName;

  LinearSales(this.year, this.sales, this.colorName);
}
