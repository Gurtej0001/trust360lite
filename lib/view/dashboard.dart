import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';
import 'package:trust360lite/view/current_buzz/current_buzz.dart';
import 'package:trust360lite/view/discussion/discussion.dart';
import 'package:trust360lite/view/knowledge_base/knowledge_base.dart';
import 'package:trust360lite/view/navigation_drawer.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
            appBar: AppBar(
                backgroundColor: colors.Colors.redDark,
                title: Text(Constants.app_name, style: TextStyle(fontSize: 20)),
                actions: <Widget>[
                  IconButton(
                      padding: EdgeInsets.all(2),
                      icon: Image(image: AssetImage('images/search.png'), height: 24, width: 24),
                      onPressed: () => NavigationController.navigateToSearch(context)),
                  IconButton(
                      padding: EdgeInsets.all(2),
                      icon: Image(image: AssetImage('images/notification.png'), height: 24, width: 24),
                      onPressed: () => NavigationController.navigateToNotification(context))
                ],
                bottom: TabBar(
                    isScrollable: true,
                    labelStyle: TextStyle(fontSize: 13.0, fontWeight: FontWeight.bold),
                    tabs: [Tab(text: "CURRENT BUZZ (306)"), Tab(text: "DISCUSSION (232)"), Tab(text: "KNOWLEDGE BASE (42)")])
                /*bottom: PreferredSize(
                preferredSize: const Size.fromHeight(48.0),
                child: Theme(
                  data: Theme.of(context).copyWith(accentColor: Colors.white),
                  child: Container(
                    height: 48.0,
                    alignment: Alignment.center,
                    child: CustomTab(),
                  ),
                ),
              ),*/
                ),
            drawer: NavigationDrawer(),
            body: TabBarView(children: <Widget>[CurrentBuzz(), Discussion(), KnowledgeBase()])));
  }
}
