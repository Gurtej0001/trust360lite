import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;
import 'package:trust360lite/utility/constants.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery
            .of(context)
            .size
            .width * 0.85,
        child: Drawer(
            elevation: 1.5,
            child: Column(children: <Widget>[
              DrawerHeader(
                  decoration: BoxDecoration(color: colors.Colors.redDark),
                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    Padding(padding: EdgeInsets.only(left: 5)),
                    CircleAvatar(backgroundImage: AssetImage('images/profile.png'), radius: 30),
                    Padding(padding: EdgeInsets.only(left: 20)),
                    Text("AG Kunal", textAlign: TextAlign.left, style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500)),
                  ])),
              Expanded(
                  child: ListView(padding: EdgeInsets.zero, children: <Widget>[
                    ListTile(
                        leading: Image(image: AssetImage('images/profile.png'), height: 24, width: 24),
                        title: Text("My Profile", style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          NavigationController.navigateToProfile(context);
                        }),
                    ListTile(
                        leading: Icon(Icons.settings),
                        title: Text("My Activity", style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          NavigationController.navigateToMyActivity(context);
                        }),
                    ListTile(
                        leading: Icon(Icons.star_border),
                        title: Text(Constants.my_favorites, style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          NavigationController.navigateToMyFavorites(context);
                        }),
                    ListTile(
                        leading: Icon(Icons.notifications_none),
                        title: Text("Notification", style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          NavigationController.navigateToNotification(context);
                        }),
                    ListTile(
                        leading: Icon(Icons.insert_emoticon),
                        title: Text("Help & Feedback", style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          NavigationController.navigateToHelpAndFeedback(context);
                        }),
                    ListTile(
                        leading: Icon(Icons.lock_open),
                        title: Text("Logout", style: TextStyle(color: Colors.grey)),
                        onTap: () {
                          Navigator.pop(context);
                          _showLogoutDialog(context);
                        })
                  ])),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                Text("Powered by", style: TextStyle(color: colors.Colors.redDark, fontWeight: FontWeight.bold, fontSize: 16)),
                SizedBox(width: 80, height: 40, child: Image.asset('images/ctm_logo.png'))
              ])
            ])));
  }


  void _showLogoutDialog(BuildContext context) {
    // flutter defined function
    showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
              title: new Text("Confirm Logout?"),
              content: new Text("Are you sure want to logout?"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                 FlatButton(
                    child: new Text("Yes"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }
                ),
                FlatButton(
                    child: new Text("No"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }
                )
              ]
          );
        }
    );
  }
}
