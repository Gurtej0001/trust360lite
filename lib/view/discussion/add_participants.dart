import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class AddParticipants extends StatefulWidget {
  @override
  _AddParticipantsState createState() => _AddParticipantsState();
}

class _AddParticipantsState extends State<AddParticipants> {
  bool autoFocus = false;
  bool isChecked = false;

  bool visibilityHorizontalList = false;
  double preferredSizeHeight = 85;

  void selectAllCheckBox(bool isChecked, bool visibilityHorizontalList, double preferredSizeHeight) {
    setState(() {
      this.isChecked = isChecked;
      this.visibilityHorizontalList = visibilityHorizontalList;
      this.preferredSizeHeight = preferredSizeHeight;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
          backgroundColor: colors.Colors.redDark,
          title: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Expanded(flex: 1, child: Text("Add Participants".toUpperCase(), style: TextStyle(fontSize: 18))),
            InkWell(
              onTap: () => NavigationController.navigateToNewDiscussion(context),
              child: Text("NEXT", style: TextStyle(fontSize: 18)),
            )
          ]),
          bottom: PreferredSize(
              child: Container(
                  color: Colors.white,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                        child: Row(children: <Widget>[
                          Expanded(flex: 1, child: Text("Select Users", style: TextStyle(color: Colors.black, fontSize: 14))),
                          InkWell(
                              onTap: () => isChecked ? selectAllCheckBox(false, false, 85.0) : selectAllCheckBox(true, true, 165.0),
                              child: Text("Select All", style: TextStyle(color: colors.Colors.redDark, fontSize: 14)))
                        ])),
                    Container(margin: EdgeInsets.all(10.0), child: searchField()),
                    Visibility(
                        visible: visibilityHorizontalList,
                        child: SizedBox(
                            height: 80.0,
                            child: ListView(scrollDirection: Axis.horizontal, children: <Widget>[
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("abdullah"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("adnan"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("ahmed.alaradi"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("ajit.ghagare"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("ali.albana"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("amal"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("amina"))
                                  ])),
                              Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                    Stack(children: <Widget>[
                                      CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, width: 100, height: 100))),
                                      Positioned(top: 0.0, left: 30.0, right: 0.0, child: Icon(Icons.clear, size: 20.0))
                                    ]),
                                    Padding(padding: const EdgeInsets.only(top: 5.0), child: Text("arsalan"))
                                  ]))
                            ])))
                  ])),
              preferredSize: Size.square(preferredSizeHeight))),
      body: ListView(
          shrinkWrap: true,
          children: ListTile.divideTiles(context: context, tiles: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("Abdullah", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("Adnan", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("ahmed.alaradi", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("ajit.ghagare", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("ali.albana", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("amal", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("amina", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("arsalan", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("asrar", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("avaiz", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("ehtisham", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("fazal", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ]),
            Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: EdgeInsets.all(10.0),
                    height: 50,
                    width: 50,
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 10,
                        child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 50, width: 50)))),
                Expanded(flex: 1, child: Text("furqan", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                Theme(data: Theme.of(context).copyWith(unselectedWidgetColor: colors.Colors.redDark), child: Checkbox(value: isChecked, onChanged: (bool value) {}))
              ])
            ])
          ]).toList()),
    );
  }

  Widget searchField() {
    return Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Icon(Icons.search, color: Colors.grey),
      Expanded(
          flex: 1,
          child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                  height: 35.0,
                  decoration: new BoxDecoration(border: new Border.all(color: Colors.grey)),
                  child: TextField(
                      autofocus: false,
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontSize: 18.0),
                      decoration:
                          InputDecoration(contentPadding: EdgeInsets.all(10.0), border: InputBorder.none, hintText: "Search", fillColor: Colors.white, filled: true),
                      onTap: () => setState(() {
                            autoFocus = !autoFocus;
                          }))))),
      Icon(Icons.clear, color: Colors.grey)
    ]);
  }
}
