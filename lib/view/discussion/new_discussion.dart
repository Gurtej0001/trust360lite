import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class NewDiscussion extends StatefulWidget {
  @override
  _NewDiscussionState createState() => _NewDiscussionState();
}

class _NewDiscussionState extends State<NewDiscussion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.arrow_left, color: Colors.white), onPressed: () => Navigator.of(context).pop()),
            backgroundColor: colors.Colors.redDark,
            title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Expanded(flex: 1, child: Text("NEW DISCUSSION", style: TextStyle(fontSize: 18))), Text("NEXT", style: TextStyle(fontSize: 18))])),
        body: Container(
            color: Colors.white,
            child: Column(children: <Widget>[
              Container(
                  color: colors.Colors.greyLight,
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(top: 10),
                          child: RawMaterialButton(
                              onPressed: () {},
                              child: new Icon(Icons.camera_alt, color: colors.Colors.greyLight, size: 35.0),
                              shape: new CircleBorder(),
                              elevation: 1.0,
                              fillColor: Colors.grey,
                              padding: const EdgeInsets.all(5.0))),
                      Flexible(
                          child: Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                TextField(
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                        hintText: "Type group subject here...*",
                                        hintStyle: TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.bold)),
                                    cursorColor: Colors.red),
                                TextField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    decoration: InputDecoration(hintText: "Description (Optional)", hintStyle: TextStyle(color: Colors.grey, fontSize: 16)),
                                    cursorColor: Colors.red)
                              ])))
                    ])
                  ]))
            ])));
  }
}
