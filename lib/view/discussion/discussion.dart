import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:trust360lite/routers/navigation_controller.dart';
import 'package:trust360lite/utility/colors.dart' as colors;

class Discussion extends StatefulWidget {
  @override
  _DiscussionState createState() => _DiscussionState();
}

class _DiscussionState extends State<Discussion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: ListView(
              shrinkWrap: true,
              children: ListTile.divideTiles(context: context, tiles: [
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Column(children: <Widget>[
                                      Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Expanded(flex: 1, child: Text("This is the test new select all discussion", maxLines: 2, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                        Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                        Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                        Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                        Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                      ]),
                                      Row(children: <Widget>[
                                        Flexible(
                                            child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                        Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                      ])
                                    ]))
                              ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold)),
                                                    Expanded(flex: 1,child: Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))),
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 10.0),
                                                      child: Container(
                                                          width: 20,
                                                          height: 20,
                                                          child: Center(child: Text("2", style: TextStyle(color: Colors.white))),
                                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green)),
                                                    )
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star, color: colors.Colors.redDark, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold)),
                                                    Expanded(flex: 1,child: Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))),
//                                                    Icon(Icons.volume_off, color: Colors.grey, size: 16),
                                                    Opacity(opacity: 0.0, child: Image.asset("images/mute.png", fit: BoxFit.cover, height: 12, width: 12)),
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 2.0),
                                                      child: Container(
                                                          width: 15,
                                                          height: 15,
                                                          child: Center(child: Image.asset("images/pin.png", fit: BoxFit.cover, height: 10, width: 10)),
                                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.grey)),
                                                    ),
                                                    Opacity(opacity: 0.0,
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left: 2.0),
                                                        child: Container(
                                                          width: 20,
                                                          height: 20,
                                                          child: Center(child: Text("2", style: TextStyle(color: Colors.white))),
                                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green))
                                                      )
                                                    )
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold)),
                                                    Expanded(flex: 1,child: Text('  |  3 months ago, zubair', overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14.0, color: Colors.grey))),
//                                                    Icon(Icons.volume_off, color: Colors.grey, size: 16),
                                                    Image.asset("images/mute.png", fit: BoxFit.cover, height: 12, width: 12),
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 5.0),
                                                      child: Container(
                                                          width: 15,
                                                          height: 15,
                                                          child: Center(child: Image.asset("images/pin.png", fit: BoxFit.cover, height: 10, width: 10)),
                                                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.grey)),
                                                    ),
                                                    Opacity(opacity: 0.0,
                                                        child: Padding(
                                                            padding: const EdgeInsets.only(left: 0.0),
                                                            child: Container(
                                                                width: 20,
                                                                height: 20,
                                                                child: Center(child: Text("2", style: TextStyle(color: Colors.white))),
                                                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green))
                                                        )
                                                    )
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ])))),
                Container(
                    child: InkWell(
                        onTap: () => NavigationController.navigateToMyFavoritesDiscussionChat(context),
                        child: Container(
                            color: Colors.white,
                            child: Row(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(bottom: 40),
                                  width: 30,
                                  height: 30,
                                  child: IconButton(icon: Icon(Icons.star_border, color: Colors.grey, size: 26.0), onPressed: () => {})),
                              Container(
                                  height: 60,
                                  width: 60,
                                  child: CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      radius: 10,
                                      child: ClipOval(child: Image.asset("images/profile.png", fit: BoxFit.cover, height: 60, width: 60))),
                                  decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: colors.Colors.redDark, width: 1.0))),
                              Flexible(
                                  child: Container(
                                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Column(children: <Widget>[
                                              Row(children: <Widget>[
                                                Expanded(flex: 1, child: Text("Private Chat with Raghu", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
                                                Icon(Icons.remove_red_eye, color: Colors.grey, size: 16),
                                                Text("34", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold)),
                                                Container(padding: EdgeInsets.only(left: 10), child: Icon(Icons.undo, color: Colors.grey, size: 16)),
                                                Text("5", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold))
                                              ]),
                                              Padding(
                                                  padding: EdgeInsets.only(top: 20.0),
                                                  child: Row(children: <Widget>[
                                                    Flexible(
                                                        child: Text("#Public", style: TextStyle(color: colors.Colors.redDark, fontSize: 14, fontWeight: FontWeight.bold))),
                                                    Text('  |  3 months ago, zubair', style: TextStyle(fontSize: 14.0, color: Colors.grey))
                                                  ]))
                                            ]))
                                      ])))
                            ]))))
              ]).toList()),
        ),
        floatingActionButton: SpeedDial(
            marginRight: 16,
            marginBottom: 16,
//            animatedIcon: AnimatedIcons.menu_close,
//            animatedIconTheme: IconThemeData(size: 22.0),
            child: Icon(Icons.more_vert),
            visible: true,
            closeManually: false,
            curve: Curves.bounceIn,
            overlayColor: Colors.black,
            overlayOpacity: 0.5,
            backgroundColor: colors.Colors.redDark,
            foregroundColor: Colors.white,
            elevation: 8.0,
            shape: CircleBorder(),
            children: [
              SpeedDialChild(
                  child: Icon(Icons.add, color: Colors.white),
                  backgroundColor: colors.Colors.redDark,
                  label: 'New Discussion',
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () => NavigationController.navigateToAddParticipants(context)),
              SpeedDialChild(
                  child: Icon(Icons.people_outline, color: Colors.white),
                  backgroundColor: colors.Colors.redDark,
                  label: 'Public Discussion',
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () => {}),
              SpeedDialChild(
                  child: Icon(Icons.lock_open, color: Colors.white),
                  backgroundColor: colors.Colors.redDark,
                  label: 'My Discussion',
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () => {})
            ]));
  }
}
